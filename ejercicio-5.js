// Ejercicio 3

const { getgroups } = require("process");

// Una empresa quiere realizar concursos para promocionar sus productos, por lo que necesita ser capaz
// de extraer de una lista de participantes varios grupos de un número variable de miembros. Escribe una función
// que les permita escoger N grupos de M participantes cada uno, de forma que devuelva grupos aleatorios, donde un 
// mismo participante no pueda formar parte de más de uno de los grupos ganadores (solo se puede ganar 1 vez).

// Puedes utilizar el siguiente array de participantes:


// const n = grupos
// const m = participantes de cada grupo

const participants = [
  {
    nick: "bigleopard803",
    name: "Hugo",
    surname: "Araújo",
    email: "hugo.araujo@example.com",
  },
  {
    nick: "yellowdog306",
    name: "Alice",
    surname: "Jean-Baptiste",
    email: "alice.jean-baptiste@example.com",
  },
  {
    nick: "beautifulbear293",
    name: "Pinja",
    surname: "Lammi",
    email: "pinja.lammi@example.com",
  },
  {
    nick: "ticklishbear115",
    name: "Bojan",
    surname: "Dumas",
    email: "bojan.dumas@example.com",
  },
  {
    nick: "organicmeercat397",
    name: "Eren",
    surname: "Ilıcalı",
    email: "eren.ilicali@example.com",
  },
  {
    nick: "organicdog481",
    name: "Emaús",
    surname: "das Neves",
    email: "emaus.dasneves@example.com",
  },
  {
    nick: "smallgorilla944",
    name: "Florence",
    surname: "Ross",
    email: "florence.ross@example.com",
  },
  {
    nick: "crazyduck454",
    name: "Lino",
    surname: "Pierre",
    email: "lino.pierre@example.com",
  },
  {
    nick: "bluefrog830",
    name: "Vilma",
    surname: "Heinonen",
    email: "vilma.heinonen@example.com",
  },
  {
    nick: "happyrabbit114",
    name: "Umut",
    surname: "Kiliççi",
    email: "umut.kilicci@example.com",
  },

];

// Ejemplo input: (participants, 2, 2)

// Ejemplo output:
// [
//   // Grupo 1
//   [
//     {
//       nick: "bluefrog830",
//       name: "Vilma",
//       surname: "Heinonen",
//       email: "vilma.heinonen@example.com",
//     },
//     {
//       nick: "smallgorilla944",
//       name: "Florence",
//       surname: "Ross",
//       email: "florence.ross@example.com",
//     },
//   ],
//   // Grupo 2
//   [
//     {
//       nick: "crazyduck454",
//       name: "Lino",
//       surname: "Pierre",
//       email: "lino.pierre@example.com",
//     },
//     {
//       nick: "yellowdog306",
//       name: "Alice",
//       surname: "Jean-Baptiste",
//       email: "alice.jean-baptiste@example.com",
//     },
//   ],
// ];



function getFloor(x) {
  console.log(Math.floor(x))
}
getFloor(5.9)

// const getWinners = (participants, n, m) => {
//   if (n === 0 || m === 0) return []

//   const participantsCopy = [...participants]
//   const winners = []

//   for(let i = 0; i <= n-1; i++) {
//       const group = []
//       for (let j = 0; j <= m-1; j++) {
//           const winnerPosition = Math.floor(Math.random () * (participantsCopy.length - 1))
//           const winner = participantsCopy.splice(winnerPosition,1)[0]
//           group[j] = winner
//         }
//         winners.push(group)
//       }
//       console.log(winners)
//       return winners
// }
// const groups = 4
// const participantsInGroup = 2
// getWinners(participants, groups, participantsInGroup)
    
    // const groups = 3
// const usersNum = 3
// // Copio el array participantes y le doy la vuelta
// const copyParticipants = [ ...participants.reverse()]
// let  randomNumber1
// // Primero creo los grupos por separado Y después dentro del arrayGroups creo el número de grupos

// const groupCreate = []
// const arrayGroups = []
// // Multiplico por  n participantes
// let winnerParticipants
// for (let i = 0; i < usersNum; i++) {
//   // Length para saber cu´ntos indices hay en el array para crear un randomNumber  y elegir a ganadores
//   const participantsNumber = participants.length
//   randomNumber1 = Math.round(Math.random() * participantsNumber)
//   // Debería hacer un filter para que cuando meta al usuario eliminarlo de la lista para no repetirlo
//   winnerParticipants = copyParticipants[i]
//   copyParticipants.slice()
//   // console.log(winnerParticipants)

//   arrayGroups.push(winnerParticipants)
// }
// // Ahora creo el primer grupo con el número de ususarios requerido
// // Al for le pongo como límite 1 pero la idea es límite groups
// for (let j = 0; j < groups; j++) {
//   groupCreate.push(arrayGroups)
// }
// // console.log(arrayGroups)
// console.log(groupCreate)

