// // // Ejercicio 3

// const { object } = require("webidl-conversions")

// // const { object } = require("webidl-conversions")

// // // Escribe una función que, al recibir un array con la forma del ejemplo a continuación,
// // //  devuelva un objeto con el número de animales de cada especie. Piensa que el array puede ser 
// // //  modificado para añadir nuevas especies que no estuviesen en el original.

// // // Ejemplo input:


// // // Ejemplo output:
// // // {
// //     // 	dog: 3,
// //     // 	cat: 4,
// // // 	chicken: 1,
// // // 	snake: 1
// // // }

const animals = [
    {
        name: "Frank",
        species: "dog",
    },
    {
        name: "Romeo",
        species: "cat",
    },
    {
        name: "Olivia",
        species: "chicken",
    },
    {
        name: "Cooper",
        species: "cat",
    },
    {
        name: "Max",
        species: "dog",
    },
    {
        name: "Oscar",
        species: "dog",
    },
    {
        name: "Lola",
        species: "cat",
    },
    {
        name: "Jax",
        species: "snake",
    },
    {
        name: "Millie",
        species: "cat",
    },
    {
        name: "Millie",
        species: "arana",
    },
    {
        name: "Millie",
        species: "arana",
    },
    {
        name: "Millie",
        species: "ballena",
    },
]


// Además yo haría una función para meter un animal y si no está en el array, que lo agrgue

const arrayTotal = []
const specieInArray = ['dog', 'cat', 'chicken', 'ballena', 'arana', 'snake']

const searchAnimals = (array1,array2) => {
    // Aquí creo una variable donde despúes declaro los valores a buscar que los recojo
    // del array original por si incrementa
    let arrayForSpecieForSeach
    // Aqui lo recorro y creo un array de objetos
    arrayForSpecieForSeach = array2.map(function(x) {
        return {species: x}
    })
    // Voy a crear otro array de objetos copia de animals por si se añaden más animales
    let copyArray
    // Aqui lo recorro y creo un array de objetos
    copyArray = array1.map(function(x) {
        return x
    })
    // Este array es para recoger información de dentro del for
    let arrayTotalForSpecies = []
    // Voy a crear un array con el total de animales por especie
    let myAnimals = {}
    for (let i = 0; i < arrayForSpecieForSeach.length; i++) {
        // Aquí filtro el array por type
        const filteredForSpecie = copyArray.filter(element => element.species === arrayForSpecieForSeach[i].species)
        // Aqui obtengo el numero total
        const totalSpecieType = filteredForSpecie.length
        // Creo un objeto con las var y las meto todas.
        myAnimals[`${arrayForSpecieForSeach[i].species}`] = totalSpecieType
    }
    arrayTotal.push(myAnimals)
   

}
searchAnimals(animals, specieInArray)
console.log(arrayTotal)

