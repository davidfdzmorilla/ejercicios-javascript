// Ejercicio 3

// Escribe una función que, dados una especie (perro, gato...) y dos arrays con la forma de los ejemplos
// a continuación, cuente la puntuación total que obtienes al sumar las puntuaciones de los animales de dicha
// especie.

// Ejemplo input: "cat".
// Ejemplo output: 47

const animalScores = [
  {
    animal: "Frank",
    score: 5,
  },
  {
    animal: "Romeo",
    score: 13,
  },
  {
    animal: "Olivia",
    score: 3,
  },
  {
    animal: "Cooper",
    score: 4,
  },
  {
    animal: "Max",
    score: 5,
  },
  {
    animal: "Oscar",
    score: 27,
  },
  {
    animal: "Lola",
    score: 14,
  },
  {
    animal: "Jax",
    score: 7,
  },
  {
    animal: "Millie",
    score: 16,
  },
];

const animals = [
  {
    name: "Frank",
    species: "dog",
  },
  {
    name: "Romeo",
    species: "cat",
  },
  {
    name: "Olivia",
    species: "chicken",
  },
  {
    name: "Cooper",
    species: "cat",
  },
  {
    name: "Max",
    species: "dog",
  },
  {
    name: "Oscar",
    species: "dog",
  },
  {
    name: "Lola",
    species: "cat",
  },
  {
    name: "Jax",
    species: "snake",
  },
  {
    name: "Millie",
    species: "cat",
  },
];

// countScores("snake", animals, animalScores); Returns -> 7


const animalPoints = []
// Creo array con animal y score
const searchAnimal = (array1, array2) => {
  array1.forEach(element => {
    for(let i = 0; i < array2.length; i ++) {
      if(element.animal === array2[i].name) {
        animalPoints.push(
          {
            name: element.animal,
            species: array2[i].species,
            score: element.score
          }
        )
      }
    }
  })
}
searchAnimal(animalScores, animals)

// Creo un array con los puntos 
const animal = 'dog'

const totalPoints = []

const pointsCount = (array) => {
  array.forEach(element => {
    if(element.species === animal) {
      totalPoints.push(element.score)
    }
  })
}
pointsCount(animalPoints)
// Y les hago el reduce
const reducer = (a, b) => a + b
console.log(totalPoints.reduce(reducer))

