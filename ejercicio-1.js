"use strict";

/**
 *  =======================
 *  ··· P E R S O N A S ···
 *  =======================
 */
const persons = [
  {
    name: "Pedro",
    age: 35,
    code: "ES",
    infected: true,
    petName: "Troski",
  },
  {
    name: "Elisabeth",
    age: 14,
    code: "UK",
    infected: true,
    petName: "Firulais",
  },
  {
    name: "Pablo",
    age: 25,
    code: "ES",
    infected: false,
    petName: "Berritxu",
  },
  {
    name: "Angela",
    age: 18,
    code: "DE",
    infected: false,
    petName: "Noodle",
  },
  {
    name: "Boris",
    age: 50,
    code: "UK",
    infected: true,
    petName: "Leon",
  },
  {
    name: "Donald",
    age: 69,
    code: "US",
    infected: false,
    petName: "Pence",
  },
  {
    name: "Pepito",
    age: 36,
    code: "ES",
    infected: false,
    petName: "Carbón",
  },
  {
    name: "Pedrito",
    age: 36,
    code: "US",
    infected: false,
    petName: "Lorito",
  },
  {
    name: 'María',
    age: 18,
    code: 'ES',
    infected: false,
    petName: 'Arañita'
  }
];


/**
 *  =======================
 *  ··· M A S C O T A S ···
 *  =======================
 */
const pets = [
  {
    petName: "Troski",
    type: "perro",
  },
  {
    petName: "Firulais",
    type: "perro",
  },
  {
    petName: "Berritxu",
    type: "loro",
  },
  {
    petName: "Noodle",
    type: "araña",
  },
  {
    petName: "Leon",
    type: "gato",
  },
  {
    petName: "Pence",
    type: "perro",
  },
  {
    petName: "Carbón",
    type: "gato",
  },
  {
    petName: "Arañita",
    type: "araña",
  },
  {
    petName: "Lorito",
    type: "loro",
  },
];

/**
 *  =======================
 *  ··· A N I M A L E S ···
 *  =======================
 */
const animals = [
  {
    type: "perro",
    legs: 4,
  },
  {
    type: "araña",
    legs: 8,
  },
  {
    type: "gato",
    legs: 4,
  },
  {
    type: "loro",
    legs: 2,
  },
  {
    type: "gallina",
    legs: 2,
  },
];

/**
 *  ===================
 *  ··· P A Í S E S ···
 *  ===================
 */
const countries = [
  {
    code: "CN",
    name: "China",
    population: 1439,
    infected: 81999,
  },
  {
    code: "US",
    name: "Estados Unidos",
    population: 331,
    infected: 112468,
  },
  {
    code: "DE",
    name: "Alemania",
    population: 83,
    infected: 56202,
  },
  {
    code: "ES",
    name: "España",
    population: 46,
    infected: 72248,
  },
  {
    code: "UK",
    name: "Reino Unido",
    population: 67,
    infected: 17301,
  },
];

/**
 *  ###########################
 *  ## E J E R C I C I O   1 ##
 *  ###########################
 *
 *  Número total de infectados del array de personas.
 *
 */


// const infectedPersons = persons.filter(persons => persons.infected === true)

// console.log(infectedPersons.length)


/**
 *  ###########################
 *  ## E J E R C I C I O   2 ##
 *  ###########################
 *
 *  Número total de infectados en el array de países.
 *
 */

// const infectedCountries = []
// for (let i = 0; i < countries.length; i++) {
//   infectedCountries.push(countries[i].infected)
// }
// const reducer = (previusValue, currntValue) => previusValue + currntValue

// console.log(infectedCountries.reduce(reducer))



/**
 *  ###########################
 *  ## E J E R C I C I O   3 ##
 *  ###########################
 *
 *  País con más infectados.
 *
 */

// countries.sort(function (a, b) {
//   if (a.infected < b.infected) {
//     return 1;
//   }
//   if (a.infected > b.infected) {
//     return -1;
//   }
//   return 0;
// })
// console.log(`El país con más infectados es: ${countries[0].name}, con un total de ${countries[0].infected} infectados.`)




/**
 *  ###########################
 *  ## E J E R C I C I O   4 ##
 *  ###########################
 *
 *  Array con el nombre de todas las mascotas.
 *
 */


// const petsName = (array) => {
//     const newArray = []
//       array.forEach(element => {
//         newArray.push(element.petName)
//       })
//       console.log(newArray)
//     }
//     petsName(pets)

    /**
     *  ###########################
     *  ## E J E R C I C I O   5 ##
     *  ###########################
     *
     *  Array de españoles con perro.
     *
     */

// Aquí creo un Array con los nombres de los perros.

// const petsType = (array) => {
//     const dogArray = []
//     array.forEach(element => {
//       if (element.type == 'perro')
//         dogArray.push({
//           petName: `${element.petName}`
//         })
//     })

//     return dogArray
// }
// const dogArray = petsType(pets)

// // Aquí comparo los nombres de los perros con los de las personas españolas

// const isDogPet = (array1, array2) => {
//   const persons = array1
//   const dogs = array2
//   // Recorro el array persons
//   for(let i = 0; i < persons.length; i++) {
//     // Recorro el array dogs
//     for (let j = 0; j < dogs.length; j++){
//       // Condiciono por nombrePerro y codex
//       if (dogs[j].petName === persons[i].petName && persons[i].code === 'ES') {
//         console.log(`${persons[i].name} es español y tiene un perro llamado ${persons[i].petName}.`)
//       }
//     }
//   }
// }

// isDogPet(persons, dogArray)


/**
 *  ###########################
 *  ## E J E R C I C I O   6 ##
 *  ###########################
 *
 *  Array con las personas. A mayores, este array debe incluír el objeto con los datos de su mascota.
 *
 *  {
 *      name: 'Pedro',
 *      age: 35,
 *      country: 'ES',
 *      infected: true,
 *      petName: {
 *          petName: 'Troski',
 *          type: 'perro',
 *      }
 *  }
 *
 */


// const petsType = (array) => {
//   const dogArray = []
//   array.forEach(element => {
//     if (element.type == 'perro')
//       dogArray.push({
//         petName: `${element.petName}`,
//         type: `${element.type}`
//       })
//   })

//   return dogArray
// }
// const dogArray = petsType(pets)

// // Aquí comparo los nombres de los perros con los de las mascotas de personas españolas

// const isDogPet = (array1, array2) => {
// const persons = array1
// const dogs = array2
// const userWithDog = []
// // Recorro el array persons
//   for(let i = 0; i < persons.length; i++) {
//     // Recorro el array dogs
//     for (let j = 0; j < dogs.length; j++){
//       // Condiciono por nombrePerro y codex
//       if (dogs[j].petName === persons[i].petName && persons[i].code === 'ES') {
//         userWithDog.push({
//           name: `${persons[i].name}`,
//           age: `${persons[i].age}`,
//           infected: `${persons[i].infected}`,
//           petName: {
//             petName: `${persons[i].petName}`,
//             type: `${dogs[j].type}`
//           }
//         })
//       }
//     }
//   }
//   console.log(userWithDog)
// }

// isDogPet(persons, dogArray)

/**
 *  ###########################
 *  ## E J E R C I C I O   7 ##
 *  ###########################
 *
 *  Número total de patas de las mascotas.
 *
 */


// const totalLegsPets = (array) => {
//   const legs = []
//   for(let i = 0; i < array.length; i++) {
//     legs.push(array[i].legs)
//   }
//   const total = (previusValue, currentValue) => previusValue + currentValue
//   console.log(legs.reduce(total))
// }
// totalLegsPets(animals)


/**
 *  ###########################
 *  ## E J E R C I C I O   8 ##
 *  ###########################
 *
 *  Array con las personas que tienen animales de 4 patas
 *
 */

// const newArrayPersons = []
// const newArrayAnimals = []
// const usersPetXLegs = []
// // Creo array con las personas añadiendo clave petType
// for (let i = 0; i < persons.length; i++) {
//   newArrayPersons.push({
//     name: `${persons[i].name}`,
//     age: `${persons[i].age}`,
//     infected: `${persons[i].infected}`,
//     petName: {
//       petName: `${persons[i].petName}`,
//       type: `${pets[i].type}`
//     }
//   })
// }

// // Creo array filtrando los animales por número de patas
// const legs = 4


// for(let i = 0; i < animals.length; i++) {
//   if(animals[i].legs === 4) {
//     newArrayAnimals.push({
//       type: `${animals[i].type}`,
//       legs: `${animals[i].legs}`
//     })
//   }
// }
// // Recorro los dos arrays nuevos de personas y animales y creo otro con el resultado.
// for (let i = 0; i < newArrayPersons.length; i++) {
//   for (let j = 0; j < newArrayAnimals.length; j++) {
//     if(newArrayPersons[i].petName.type === newArrayAnimals[j].type) {
//     usersPetXLegs.push( {
//       name: `${newArrayPersons[i].name}`,
//       petName: {
//         typePet: `${newArrayPersons[i].petName.type}`,
//         legs: `${newArrayAnimals[j].legs}`
//       }
//     })
//   }
//   }
// }

// console.log(usersPetXLegs)

/**
 *  ###########################
 *  ## E J E R C I C I O   9 ##
 *  ###########################
 *
 *  Array de países que tienen personas con loros como mascota.
 *
 */


// const petNames = []
// const petType = 'loro'
// const filterPet = (array, pet) => {
//   array.forEach(element => {
//     if(element.type === pet) {
//       petNames.push(element.petName)
//     }
//   })
// }
// filterPet(pets, petType)

// const userCountries = []
// const filterCountries = (array1, array2) => {
//   for (let i = 0; i < array2.length; i++) {
//     array1.forEach(element => {
//       if(element.petName === array2[i]) {
//         userCountries.push(element.code)
//       }
//     })
//   }
// }
// filterCountries(persons, petNames)
// console.log(userCountries)






/**
 *  #############################
 *  ## E J E R C I C I O   1 0 ##
 *  #############################
 *
 *  Número de infectados totales (en el array de países) de los países con mascotas de ocho patas.
 *
 */
// const legs = 8
// // Creo array con animales con esas patas
// const animalWithLegs = []
// const haveLegs = (array) => {
//   // Recorro con for each y aprovecho para hacerle un condicional y agregar al array
//   array.forEach(element => {
//     if(element.legs === legs) {
//       animalWithLegs.push(element.type)
//     }
//   })
// }
// haveLegs(animals)

// // Saber nombre de pet
// let namesPet = []
// const namePets = (array1, array2) => {
//   // Si fueran mas items en el array  ---> recorro el for para que index de array2 sea variable
//   for (let i = 0; i < array2.length; i++) {
//     array1.forEach(element => {
//     if(element.type === array2[i]) {
//       namesPet.push(element.petName)
//     }
//   })
//   }
// }
// namePets(pets, animalWithLegs)

// const userCountry = []

// const usersCountries = (array1, array2) => {
//   for (let i = 0; i < array2.length; i++) {
//     array1.forEach(element => {
//       if(element.petName === array2[i]) {
//         userCountry.push(element.code)
//       }
//     })
//   }
// }
// usersCountries(persons, namesPet)
// let totalInfected
// const totalInfectedCountry = (array1, array2) => {
//   const totalInfectedCountries = []
//   for (let i = 0; i < array2.length; i++) {
//     array1.forEach(element => {
//       if(element.code === array2[i]) {
//         totalInfectedCountries.push(element.infected)
//       }
//     })
//   }
//   // Uso reduce para sumar los valores
//   totalInfected = totalInfectedCountries.reduce((a, b) => a + b, 0)
// }
// totalInfectedCountry(countries, userCountry)
// console.log(totalInfected)