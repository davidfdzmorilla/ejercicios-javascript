/**
 * ##################
 * ## Ejercicio 2 ##
 * ##################
 *
 * Utiliza los métodos map, filter o reduce para resolver las siguientes propuestas:
 *
 *  - 1. Obtén la suma total de todas las edades de las personas.
 *  - 2. Obtén la suma total de todas las edades de las personas francesas.
 *  - 3. Obtén un array con el nombre de todas las mascotas.
 *  - 4. Obtén un array con las personas que tengan gato.
 *  - 5. Obtén un array con los coches de los españoles.
 *  - 6. Obtén un array con las personas que tengan un coche de la marca Ford.
 *  - 7. ¡Bonus point! Obtén un array con todas las personas en el que cada persona tenga toda
 *       la info de su coche. Ejemplo a continuación:
 *
 *      [
 *          {
 *               name: 'Berto',
 *               country: 'ES',
 *               age: 44,
 *               car: {
 *                  id: 'LU9286V',
 *                  brand: 'Citroen',
 *                  model: 'Xsara'
 *               },
 *               pet: {
 *                   name: 'Moon',
 *                   type: 'perro'
 *               }
 *           },
 *           (...)
 *      ]
 *
 *  Tip: en algún caso es probable que el método "nombreArray.find()" te sea de ayuda.
 *
 */

"use strict";

const { object } = require("webidl-conversions");

const persons = [
  {
    name: "Berto",
    country: "ES",
    age: 44,
    car: "LU9286V",
    pet: {
      name: "Moon",
      type: "perro",
    },
  },
  {
    name: "Jess",
    country: "UK",
    age: 29,
    car: "GB2913U",
    pet: {
      name: "Kit",
      type: "gato",
    },
  },
  {
    name: "Tom",
    country: "UK",
    age: 36,
    car: "GB8722N",
    pet: {
      name: "Rex",
      type: "perro",
    },
  },
  {
    name: "Alexandre",
    country: "FR",
    age: 19,
    car: "FT5386P",
    pet: {
      name: "Aron",
      type: "gato",
    },
  },
  {
    name: "Rebeca",
    country: "ES",
    age: 32,
    car: "MD4578T",
    pet: {
      name: "Carbón",
      type: "gato",
    },
  },
  {
    name: "Stefano",
    country: "IT",
    age: 52,
    car: "LP6572I",
    pet: {
      name: "Bimbo",
      type: "perro",
    },
  },
  {
    name: "Colette",
    country: "FR",
    age: 22,
    car: "FU8929P",
    pet: {
      name: "Amadeu",
      type: "gato",
    },
  },
];

const cars = [
  {
    id: "LU9286V",
    brand: "Citroen",
    model: "Xsara",
  },
  {
    id: "GB2913U",
    brand: "Fiat",
    model: "Punto",
  },
  {
    id: "GB8722N",
    brand: "Opel",
    model: "Astra",
  },
  {
    id: "FT5386P",
    brand: "Ford",
    model: "Focus",
  },
  {
    id: "MD4578T",
    brand: "Opel",
    model: "Corsa",
  },
  {
    id: "LP6572I",
    brand: "Ford",
    model: "Fiesta",
  },
  {
    id: "FU8929P",
    brand: "Fiat",
    model: "Uno",
  },
];

// 1. Obtén la suma total de todas las edades de las personas.

// let totalUsersAge
// const sumaEdad = (array) => {
//   const age = []
//   array.forEach(element => {
//     age.push(element.age)
//   })
//   totalUsersAge = age.reduce((a, b) => a + b, 0)
// }
// sumaEdad(persons)
// console.log(totalUsersAge)





// 2. Obtén la suma total de todas las edades de las personas francesas.

// let totalAge
// const nationality = 'FR'
// const sumaEdad = (array, country) => {
//   const agesFrenchUser = []
//   const frenchUser = array.filter(code => code.country === country)
//   frenchUser.forEach(element => {
//     agesFrenchUser.push(element.age)
//   })
//   totalAge = agesFrenchUser.reduce((a, b) => a + b, 0)
// }
// sumaEdad(persons, nationality)
// console.log(totalAge)

// 3. Obtén un array con el nombre de todas las mascotas.

// const petsName = []

// const names = (array) => {
//   array.forEach(element => {
//     petsName.push(element.pet.name)
//   })
// }
// names(persons)
// console.log(petsName)





// 4. Obtén un array con las personas que tengan gato.

// const userWithCat = []
// const petType = 'gato'
// const filterForPet = (array, pet) => {
//   array.forEach(element => {
//     if (element.pet.type === pet) {
//       userWithCat.push(element.name)
//     }
//   })
// }
// filterForPet(persons, petType)
// console.log(userWithCat)




// 5. Obtén un array con los coches de los españoles.

// const spUsersCars = []
// const codeCountryUser = 'ES'
// const filterCar = (array1, array2, country) => {
//   const usersSpCarsId = array1.filter(element => element.country === country)
//   const [{ id , brand }] = array2
//   const spCarsModel = []
//   array2.forEach(element => {
//     for (let i = 0; i < usersSpCarsId.length; i++) {
//       if (element.id === usersSpCarsId[i].car) {
//         spCarsModel.push(element.model)
//       }
//     }
//   })
//   console.log(spCarsModel)
// }
// filterCar(persons, cars, codeCountryUser)







// 6. Obtén un array con las personas que tengan un coche de la marca Ford.





// const brand = 'Ford'
// const fordCards = cars.filter(element => element.brand === brand)
// const usersFord = []
// const isBrand = (array1, array2) => {
//   for (let i = 0; i < array1.length; i++) {
//     array2.forEach(element => {
//       if(array1[i].car === element.id) {
//         usersFord.push(array1[i].name)
//       }
//     })
//   }
// }
// isBrand(persons, fordCards)
// console.log(usersFord)



// 7. ¡Bonus point! Obtén un array con todas las personas en el que cada persona tenga toda
//    la info de su coche.

let newArrayUsers = []
const formattedArray = (array1, array2) => {
  for (let i = 0; i < array1.length; i++) {
    array2.forEach(element => {
      if (element.id === array1[i].car) {
        newArrayUsers.push(
          {
          name: array1[i].name,
          country: array1[i].country,
          age: array1[i].age,
          car: {
            id: element.id,
            brand: element.brand,
            model: element.model,
          },
          pet: {
          name: array1[i].pet.name,
          type: array1[i].pet.type,
          },
        })
      }
    })
  }
}
formattedArray(persons, cars)
console.log(newArrayUsers)
